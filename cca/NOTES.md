# Build Environment

Ubuntu 22.04

# Dependencies

The `reference-value-provider-service` module needs `protoc(1)`.

```sh
sudo apt-get install protobuf-compiler
```

# Patch

```sh
git checkout clo/cca
git pull
```

# Build

> **Note:** we always build separate AS and RVPS services.

Set `TRUSTEE_SRC` according to your local setup.  For me:

```sh
export TRUSTEE_SRC="/home/tho/Code/git.codelinaro.org/linaro/dcap/cca-demos/coco-trustee"
```

## gRPC Attestation Service

`aarch64` is not supported in the default build.  We need to deselect all features and only enable what is strictly needed.

```sh
cd "${TRUSTEE_SRC}/attestation-service"

cargo build \
  --bin grpc-as \
  --no-default-features \
  --features cca-verifier,rvps-grpc,grpc-bin,reference-value-provider-service
```

## RVPS & RVPS tool

```sh
make -C "${TRUSTEE_SRC}/attestation-service/rvps"
```

## Key Broker Service

```sh
make -C "${TRUSTEE_SRC}/kbs" background-check-kbs AS_TYPE=coco-as-grpc
```

# Client

N/A.  The (emulated) client is a shell script.

# Run

## Workdir Setup

```sh
CCDIR="/opt/confidential-containers"

sudo mkdir -p "${CCDIR}"
sudo chown $(id -un):$(id -gn) "${CCDIR}"
```

## `/etc/hosts` Setup

A few service names must be configured:

`rvps` and `grpc-as`:

```sh
sudo sh -c 'printf "# confidential containers\n127.0.0.1 rvps grpc-as\n" >> /etc/hosts'
```

`verification-service.veraison-net` too:

```sh
sudo sh -c 'printf "# veraison services\nINSERT_VERAISON_IPADDR_HERE verification-service.veraison-net\n" >> /etc/hosts'
```

> **Note:** `verification-service.veraison-net` matches one of the SAN names in the [veraison "test" certificate](https://github.com/veraison/services/blob/main/deployments/docker/src/certs/verification.crt).  If your veraison service endpoint uses a custom name and certificate, the instructions given above must be modified accordingly.

# KBS

```sh
RUST_LOG=debug ${TRUSTEE_SRC}/target/release/kbs -c ${TRUSTEE_SRC}/kbs/config/kbs-config-grpc.toml
```

## RVPS

RVPS MUST start before AS.

```sh
RUST_LOG=debug ${TRUSTEE_SRC}/target/release/rvps -c ${TRUSTEE_SRC}/kbs/config/rvps.json
```

## AS

local verifier:

```sh
CCA_CONFIG_FILE=${TRUSTEE_SRC}/cca/misc/cca-config-local.json \
RUST_LOG=debug \
  ${TRUSTEE_SRC}/target/debug/grpc-as \
    -c ${TRUSTEE_SRC}/cca/misc/as-config.json \
    -s 127.0.0.1:50004
```

remote verifier:

```sh
CCA_CONFIG_FILE=${TRUSTEE_SRC}/cca/misc/cca-config-remote.json \
RUST_LOG=debug \
  ${TRUSTEE_SRC}/target/debug/grpc-as \
    -c ${TRUSTEE_SRC}/cca/misc/as-config.json \
    -s 127.0.0.1:50004
```

> **Note:** If the veraison service instance is not serving over HTTPS, modify the URI scheme of the `"origin"` configuration variable in `${TRUSTEE_SRC}/cca/misc/cca-config-remote.json` from `https` to `http`.

## KBC simulator

```sh
${TRUSTEE_SRC}/cca/scripts/kbs-client.sh
```

# Veraison Services

* [Install docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

* [Post-install docker](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

* Clone the Veraison services repo locally:

```sh
git clone https://github.com/veraison/services
```

* Build the docker environment:

```sh
cd services

make docker-deploy
```

If everything is fine the Veraison service cluster is running.

Source the control functions (for bash):

```sh
source deployments/docker/env.bash
```

and explore what you can do:

```sh
veraison -h
```

# Configure Reference Values

To configure Veraison services with the needed endorsements, clone the `poc-endorser` repository:

```sh
git clone https://git.codelinaro.org/linaro/dcap/cca-demos/poc-endorser.git
```

and follow the instructions in [README.md](https://git.codelinaro.org/linaro/dcap/cca-demos/poc-endorser/-/blob/master/README.md)


# Development Setup

```mermaid
flowchart
  grpc-AS --> Veraison::verifier
  RVPS -.- grpc-AS
  grpcurl --> grpc-AS

  cocli --> Veraison::provisioner
```

