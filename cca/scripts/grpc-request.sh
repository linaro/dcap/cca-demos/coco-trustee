#!/bin/bash

set -eux
set -o pipefail

TRUSTEE_SRC="$(dirname $0)/../.."

grpcurl \
  -plaintext \
  -import-path ${TRUSTEE_SRC}/attestation-service/protos \
  -proto attestation.proto \
  -d @ \
  grpc-as:50004 \
  attestation.AttestationService/AttestationEvaluate << EOF
$(cat ${TRUSTEE_SRC}/cca/misc/cca-grpc-request.json)
EOF
